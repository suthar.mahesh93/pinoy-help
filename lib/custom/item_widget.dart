import 'package:PinoyHelpHotline/contansts/color_constant.dart';
import 'package:PinoyHelpHotline/contansts/constant.dart';
import 'package:PinoyHelpHotline/contansts/style_constant.dart';
import 'package:PinoyHelpHotline/screen/post_detail/post_detail_screen.dart';
import 'package:PinoyHelpHotline/util/sharedpreference_util.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

class ItemWidget extends StatelessWidget {
  DocumentSnapshot documentSnapshot;
  Function(String) onClick;
  int index;


  ItemWidget({
    required this.documentSnapshot,
    required this.index,
    required this.onClick,
  });

  @override
  Widget build(BuildContext context) {
    bool isFavItem =
        SharedPreferencesUtil.getStringList(ConstantUtil.PREF_FAV_ITEMS)
            .contains(documentSnapshot["id"]);
    // TODO: implement build
    return Container(
      padding: EdgeInsets.only(top: 8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(
          color: Colors.grey.shade300,
          width: 1,
        ),
      ),
      child: AnimationConfiguration.staggeredList(
        position: index,
        duration: const Duration(milliseconds: 375),
        child: SlideAnimation(
            verticalOffset: 40.0,
            child: FadeInAnimation(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    // color:_isSelected?Colors.red:null,
                    padding: const EdgeInsets.only(right: 5),
                    alignment: Alignment.topRight,
                    child: GestureDetector(
                      onTap: () {
                        onClick(documentSnapshot["id"]);
                      },
                      child: Icon(
                        isFavItem ? Icons.favorite : Icons.favorite_border,
                        color:
                            isFavItem ? ColorUtil.colorSecondary : Colors.grey,
                        size: 18,
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                PostDetailScreen(snapshot: documentSnapshot)),
                      );
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 10, left: 10, right: 10, bottom: 10),
                      child: CachedNetworkImage(
                        height: 100,
                        width: double.infinity,
                        imageUrl:  documentSnapshot["images"][0],
                        fit: BoxFit.cover,
                        progressIndicatorBuilder:
                            (context, url,
                            downloadProgress) =>
                            Center(
                              child: SizedBox(
                                width: 30.0,
                                height: 30.0,
                                child:
                                CircularProgressIndicator(
                                  value:
                                  downloadProgress
                                      .progress,
                                  color: ColorUtil.colorPrimary,
                                ),
                              ),
                            ),
                        errorWidget: (context,
                            url, error) =>
                            Icon(Icons.error,
                                color: ColorUtil.colorPrimary),
                      ),
                    ),
                  ),

                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0, left: 8),
                          child: Text(
                            documentSnapshot["title"],
                            maxLines: 1,
                            style: fontStyleNormal.copyWith(
                                color: Colors.black54,
                                fontSize: 13),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 2, left: 8, bottom: 5),
                          child: Text(
                            "\$" + " " + documentSnapshot["expectedPrice"],
                            style: fontStyleNormal.copyWith(
                              color: Colors.black,
                              fontSize: 14,
                            ),
                          ),
                        ),
                        Divider(
                          height: 1,
                          color: Colors.grey,
                        ),
                        Center(
                            child: Padding(
                          padding: const EdgeInsets.only(top: 6.0),
                          child: Text(
                            "Call Now",
                            style: fontStyleMedium.copyWith(
                              color: ColorUtil.colorPrimary,
                              fontSize: 16,
                            ),
                          ),
                        ))
                      ],
                    ),
                  ),
                ],
              ),
            )),
      ),
    );
  }
}
