import 'package:PinoyHelpHotline/contansts/color_constant.dart';
import 'package:PinoyHelpHotline/contansts/style_constant.dart';
import 'package:flutter/material.dart';




//http://hafizmokhtar.com/tutorials/creating-custom-dropdown-button-in-flutter/
class CustomDropdown<T> extends StatelessWidget {
  final List<DropdownMenuItem<T>>? dropdownMenuItemList;
  late final ValueChanged<T?>? onChanged;
  final T? value;
  final bool isEnabled;
  final bool readOnly;

  CustomDropdown(
      {Key? key,
      this.dropdownMenuItemList,
      required this.onChanged,
      this.value,
      this.isEnabled = true,
      required this.readOnly,
      })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
   /*     Padding(
          padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
          child: Text(labelText, style: styleInputLabel),
        ),*/
        IgnorePointer(
          ignoring: !isEnabled,
          child: Container(
            height: inputHeight,
            margin: const EdgeInsets.only(top: 10.0),
            decoration: BoxDecoration(
              color: readOnly ? Colors.transparent : ColorUtil.colorWhite,
              borderRadius: BorderRadius.all(Radius.circular(5)),
              border: Border.all(
                color: ColorUtil.colorItem,
                width: 0.5,
              ),
            ),
            child: DropdownButtonHideUnderline(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(8.0, 0, 16, 0),
                child: DropdownButton(
                  isExpanded: true,
                  dropdownColor: ColorUtil.colorWhite,
                  icon: Image.asset('assets/images/dropdown_arrow.png'),
                  // itemHeight: 50.0,
                  style: styleInput,
                  items: dropdownMenuItemList,
                  onChanged: onChanged,
                  value: value,
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
