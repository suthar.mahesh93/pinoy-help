import 'package:PinoyHelpHotline/contansts/color_constant.dart';
import 'package:PinoyHelpHotline/contansts/style_constant.dart';
import 'package:PinoyHelpHotline/screen/edit_post/edit_post_screen.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

class MyItemWidget extends StatelessWidget {
  DocumentSnapshot documentSnapshot;
  Function(String) onClick;
  int index;

  MyItemWidget({
    required this.documentSnapshot,
    required this.index,
    required this.onClick,
  });

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      padding: EdgeInsets.only(top: 8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(
          color: Colors.grey.shade300,
          width: 1,
        ),
      ),
      child: AnimationConfiguration.staggeredList(
        position: index,
        duration: const Duration(milliseconds: 375),
        child: SlideAnimation(
            verticalOffset: 40.0,
            child: FadeInAnimation(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => EditPostScreen(
                                  documentSnapshot: documentSnapshot,
                                )),
                      );
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 10, left: 10, right: 10, bottom: 10),
                      child: CachedNetworkImage(
                        height: 100,
                        width: double.infinity,
                        imageUrl: documentSnapshot["images"][0],
                        fit: BoxFit.cover,
                        progressIndicatorBuilder:
                            (context, url, downloadProgress) => Center(
                          child: SizedBox(
                            width: 30.0,
                            height: 30.0,
                            child: CircularProgressIndicator(
                              value: downloadProgress.progress,
                              color: ColorUtil.colorPrimary,
                            ),
                          ),
                        ),
                        errorWidget: (context, url, error) =>
                            Icon(Icons.error, color: ColorUtil.colorPrimary),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.only(left: 0, right: 0, top: 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0, left: 8),
                            child: Text(
                              documentSnapshot["title"],
                              maxLines: 1,
                              style: fontStyleNormal.copyWith(
                                  color: Colors.black54, fontSize: 13),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 2, left: 8, bottom: 5),
                            child: Text(
                              "\$" + " " + documentSnapshot["expectedPrice"],
                              style: fontStyleNormal.copyWith(
                                color: Colors.black,
                                fontSize: 14,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )),
      ),
    );
  }
}
