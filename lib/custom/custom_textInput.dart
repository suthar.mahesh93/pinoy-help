import 'package:PinoyHelpHotline/contansts/color_constant.dart';
import 'package:PinoyHelpHotline/contansts/style_constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

//https://github.com/weblineindia/Flutter-TextInput/blob/master/lib/screens/widgets/custom_text_input.dart
class CustomTextInput extends StatefulWidget {
  const CustomTextInput(
      {required this.hintTextString,
      required this.textEditController,
      required this.inputType,
      this.maxLength,
      required this.readOnly,
      this.enabled,
      this.checkbox,
      this.rightIcon});

  // ignore: prefer_typing_uninitialized_variables
  final hintTextString;
  final TextEditingController textEditController;
  final InputType inputType;
  final int? maxLength;
  final bool readOnly;
  final bool? enabled;
  final Checkbox? checkbox;
  final String? rightIcon;

  @override
  _CustomTextInputState createState() => _CustomTextInputState();
}

// input text state
class _CustomTextInputState extends State<CustomTextInput> {
  // build method for UI rendering
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        /*   Padding(
          padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
          child: Text(widget.labelText, style: styleInputLabel),
        ),*/
        Container(
          height: inputHeight,
          margin: const EdgeInsets.only(top: 10.0),
          decoration: BoxDecoration(
            color: widget.readOnly ? Colors.transparent : ColorUtil.colorWhite,
            borderRadius: BorderRadius.all(Radius.circular(5)),
            border: Border.all(
              color: Colors.black, //                   <--- border color
              width: 0.5,
            ),
          ),
          child: Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(8.0, 0, 8, 0),
                  child: TextField(
                    textCapitalization: TextCapitalization.sentences,
                    readOnly: widget.readOnly,
                    enabled: widget.enabled ?? true,
                    controller: widget.textEditController,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: widget.hintTextString as String,
                      hintStyle: fontStyleNormal.copyWith(
                        color: ColorUtil.colorInputHint,
                        fontSize: 16,
                      ),
                      counterText: "",
                    ),
                    keyboardType: getInputType(),
                    style: fontStyleNormal.copyWith(
                      color: ColorUtil.colorInputText,
                      fontSize: 16,
                    ),
                    maxLines: 1,
                    maxLength: widget.inputType == InputType.PaymentCard
                        ? 19
                        : widget.maxLength ?? getMaxLength(),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  // return input type for setting keyboard
  TextInputType getInputType() {
    switch (widget.inputType) {
      case InputType.Default:
        return TextInputType.text;

      case InputType.Email:
        return TextInputType.emailAddress;

      case InputType.Number:
        return TextInputType.number;

      case InputType.PaymentCard:
        return TextInputType.number;

      default:
        return TextInputType.text;
    }
  }

  // get max length of text
  int getMaxLength() {
    switch (widget.inputType) {
      case InputType.Default:
        return 36;

      case InputType.Email:
        return 36;

      case InputType.Number:
        return 10;

      case InputType.Password:
        return 24;

      case InputType.PaymentCard:
        return 19;

      default:
        return 36;
    }
  }
}

//input types
enum InputType { Default, Email, Number, Password, PaymentCard }
