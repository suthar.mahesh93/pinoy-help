class StringUtil {
  static final String ERR_PHONE_INVALID =
      "The provided phone number is not valid.";
  static final String SUCC_OTP_SENT = "OTP successfully sent.";
  static final String ERR_OTP_TIMEOUT = "OTP time out.";

  //Add Script
  static const ERR_MISSING_TARGET_ONE = "Please enter target 1";
  static const ERR_MISSING_TARGET_TWO = "Please enter target 2";
  static const ERR_MISSING_TARGET_THREE = "Please enter target 3";
  static const ERR_MISSING_SCRIPT_NAME = "Please enter script name";
  static const ERR_MISSING_STOP_LOSS = "Please enter stop loss";
  static const ERR_MISSING_REMARK = "Please enter remark";
  static const ERR_MISSING_DATE = "Please select date";

  //Profile
  static const ERR_MISSING_FNAME = "Please enter first name";
  static const ERR_MISSING_LNAME = "Please enter last name";

  static const ERR_MISSING_PHONE = "Please enter phone.";

  //Add Post
  static const ERR_MISSING_POST_URL = "Please upload document";
  static const ERR_MISSING_POST_TITLE = "Please enter post title";
  static const ERR_MISSING_FIELDS = "Please add details";
  static const ERR_MISSING_ACTUAL_PRICE = "Please enter price";
  static const ERR_MISSING_EXCEPTED_PRICE = "Please enter excepted price";
  static const ERR_MISSING_DESCRIPTION = "Please enter description";
  static const ERR_MISSING_IMAGES = "Please upload at least one image";
}
