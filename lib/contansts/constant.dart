class ConstantUtil {


  static String FACEBOOK_URL =
      "";
  static String INSTA_URL =
      "";

  static String TERM_URL =
      "";
  //FIREBASE TABLES
  static String FIREBASE_TABLE_USERS = "users";
  static String FIREBASE_TABLE_POSTS = "posts";

  static String FIREBASE_KEY_USER_UUID = "userId";
  static String FIREBASE_KEY_CREATED_AT = "createdAt";
  static String FIREBASE_KEY_UPDATED_AT = "updatedAt";

  //Add Post screen
  static String FIREBASE_KEY_TITLE = "title";
  static String FIREBASE_KEY_PHONE = "phone";
  static String FIREBASE_KEY_PRICE = "price";
  static String FIREBASE_KEY_DESCRIPTION = "description";
  static String FIREBASE_KEY_CURRENCY = "currency";
  static String FIREBASE_KEY_TYPE = "type";
  static String FIREBASE_KEY_CURRENCY_CODE = "currencyCode";



  static String FIREBASE_KEY_URL = "images";
  static String FIREBASE_KEY_FNAME = "fname";
  static String FIREBASE_KEY_LNAME = "lname";
  static String FIREBASE_KEY_PROFILE = "profile";
  static String FIREBASE_KEY_GENDER = "gen";
  static String FIREBASE_KEY_STATUS = "status";
  static String FIREBASE_KEY_FILE_TYPE = "fileType";

  static String FIREBASE_KEY_APP_VERSION = "appVersion";
  static String FIREBASE_KEY_OS_VERSION = "osVersion";
  static String FIREBASE_KEY_DEVICE_TYPE = "deviceType";
  static String FIREBASE_KEY_DEVICE_MODAL = "deviceModel";
  static String FIREBASE_KEY_DEVICE_BRAND = "deviceBrand";
  static String FIREBASE_KEY_PUSH_TOKEN = "pushToken";
  static String FIREBASE_IS_FREE_USER = "isFreeUser";



  static String PREF_IS_FREE_USER = "PREF_IS_FREE_USER";



  //FIREBASE ERROR CODES
  static String FIREBASE_ERR_CODE_INVALID = "invalid-phone-number";

  static String PREF_ISLOGIN = "PREF_ISLOGIN";
  static String PREF_LOGIN_USER = "PREF_LOGIN_USER";
  static String PREF_LOGIN_USER_UUID = "PREF_LOGIN_USER_UUID";
  static String PREF_LOGIN_USER_PHONE = "PREF_LOGIN_USER_PHONE";

  static String PREF_APP_VERSION = "PREF_APP_VERSION";
  static String PREF_FAV_ITEMS = "PREF_FAV_ITEMS";

  static String PREF_FNAME = "PREF_FNAME";
  static String PREF_LNAME = "PREF_LNAME";
  static String PREF_PROFILE = "PREF_PROFILE";
  static String PREF_ROLE = "PREF_ROLE";

  static String PREF_OS_VERSION = "PREF_OS_VERSION";
  static String PREF_DEVICE_TYPE = "PREF_DEVICE_TYPE";
  static String PREF_DEVICE_MODEL = "PREF_DEVICE_MODEL";
  static String PREF_DEVICE_BRAND = "PREF_DEVICE_BRAND";

  static int RECORD_ACTIVE = 1;
  static int RECORD_DELETED = 0;

}
