import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'color_constant.dart';

const buttonHeight = 45.0;
const inputHeight = 45.0;
const appElevation = 2.0;
final TextStyle fontStyleNormal = GoogleFonts.workSans(
  fontSize: 18,
  fontWeight: FontWeight.w400,
  color: Color(0xff040415),
);

final TextStyle fontStyleMedium = GoogleFonts.workSans(
  fontSize: 18,
  fontWeight: FontWeight.w500,
  color: Color(0xff040415),
);

final TextStyle fontStyleSemiBold = GoogleFonts.workSans(
  fontSize: 18,
  fontWeight: FontWeight.w600,
  color: Color(0xff040415),
);

final TextStyle fontStyleExtraBold = GoogleFonts.workSans(
  fontSize: 18,
  fontWeight: FontWeight.w800,
  color: Color(0xff040415),
);

final styleLetsTitle = fontStyleMedium.copyWith(
  color: Colors.black87,
  fontSize: 24,
);

final styleGetTitle = fontStyleExtraBold.copyWith(
  color: Colors.black87,
  fontSize: 24,
);

final styleHomeDes = fontStyleMedium.copyWith(
  color: ColorUtil.colorInputHint,
  fontSize: 13,
);
final styleprice = fontStyleMedium.copyWith(
  color: Colors.black54,
  fontSize: 14,
);

final styleItemBuy = fontStyleSemiBold.copyWith(
  color: ColorUtil.colorPrimary,
  fontSize: 14,
);

final styleDetailTitle = fontStyleSemiBold.copyWith(
  color: const Color(0xff3B3B3B),
  fontSize: 26,
);

final styleExpPriceTitle = fontStyleMedium.copyWith(
  color: const Color(0xff3B3B3B),
  fontSize: 20,
);

final styleActualPrice = fontStyleMedium.copyWith(
  decoration: TextDecoration.lineThrough,
  color: const Color(0xffBBBBBB),
  fontSize: 16,
);

final styleMobileNumber = fontStyleSemiBold.copyWith(
  color: const Color(0xff3B3B3B),
  fontSize: 16,
);

final stylePhone = fontStyleSemiBold.copyWith(
  color: const Color(0xffBBBBBB),
  fontSize: 14,
);

final styleProductDetailText = fontStyleSemiBold.copyWith(
  color: const Color(0xff3B3B3B),
  fontSize: 16,
);

final styleDescription = fontStyleSemiBold.copyWith(
  color: const Color(0xffBBBBBB),
  fontSize: 14,
);

final styleCallNow = fontStyleSemiBold.copyWith(
  color: Colors.white,
  fontSize: 18,
);

final styleTabTitle = fontStyleNormal.copyWith(
  color: Colors.white,
  fontSize: 12,
);

final styleDesc = fontStyleNormal.copyWith(
  color: ColorUtil.colorPreloginTitle,
  fontSize: 18,
);

final styleTitle = fontStyleNormal.copyWith(
     fontSize: 20, color: ColorUtil.colorWhite);

final styleUserName = fontStyleSemiBold.copyWith(
  fontSize: 18,
  color: Colors.grey,
);
final styleUserPhone = fontStyleMedium.copyWith(
  fontSize: 15,
  color: Colors.grey,
);

final styleButton = fontStyleMedium.copyWith(
  color: Colors.white,
  fontSize: 15,
);

final styleText = fontStyleMedium.copyWith(
  color: ColorUtil.colorPrimary,
  fontSize: 13,
);

final styleInputLabel = fontStyleMedium.copyWith(
  color: ColorUtil.colorSecondary,
  fontSize: 16,
);

final styleInput = fontStyleNormal.copyWith(
  color: ColorUtil.colorInputText,
  fontSize: 16,
);

final styleInputHint = fontStyleNormal.copyWith(
  color: ColorUtil.colorInputHint,
  fontSize: 16,
);

final styleAlertTitle = fontStyleMedium.copyWith(
  fontSize: 15,
  color: ColorUtil.colorPrimary,
);

final styleAlertYes = fontStyleMedium.copyWith(
  fontSize: 15,
  color: ColorUtil.colorPrimary,
);

final styleAlertNo = fontStyleMedium.copyWith(
  fontSize: 15,
  color: ColorUtil.colorPrimary,
);

final styleLoginInput = fontStyleMedium.copyWith(
  color: ColorUtil.colorPrimary,
  fontSize: 16,
);

final styleUploadImagePdf = fontStyleMedium.copyWith(
  fontSize: 15,
  color: ColorUtil.colorPrimary,
);
