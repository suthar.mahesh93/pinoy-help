import 'dart:ui';

class ColorUtil {
  static const Color backgroundColor = const Color(0x90b47272);
  static const Color bgColor = const Color(0xFF223B4F);

  static const Color colorPrimary = const Color(0xffdb0000);
  static const Color colorWhite = const Color(0xFFffffff);
  static const Color colorItem = const Color(0x90656464);

  static const Color colorSecondary = const Color(0xFF004394);

  static const Color colorRed = const Color(0xffff0000);
  static const Color colorOrange = const Color(0xffFF9011);
  static const Color colorGreen = const Color(0xff0CC443);

  static const Color colorInputText = const Color(0xff454F63);
  static const Color colorInputHint = const Color(0xffA2A5B5);
  static const Color colorPreloginTitle = const Color(0xFF171717);
  static const Color colorPreloginInputHint = const Color(0xFF8F92A1);
}
