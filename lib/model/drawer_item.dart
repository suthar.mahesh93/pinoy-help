import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// file : "success"
/// fileUrl : "5447"

class DrawerItem {
  String _name = "";

  String get name => _name;

  IconData get icon => _icon;
  IconData _icon = Icons.arrow_back;

  DrawerItem({required String name, required IconData icon}) {
    _icon = icon;
    _name = name;
  }

  // DrawerItem.fromJson(dynamic json) {
  //   _icon = json["_icon"];
  //   _name = json["name"];
  // }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["icon"] = _icon;
    map["name"] = _name;
    return map;
  }
}
