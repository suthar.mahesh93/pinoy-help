


class CurrencyItem {
  String _currency = "";
  String _code = "";

  String get currency => _currency;

  String get code => _code;

  CurrencyItem({required String currency, required String code}) {
    _currency = currency;
    _code = code;
  }

  CurrencyItem.fromJson(dynamic json) {
    print(json);
    _currency = json["currency"];
    _code = json["code"];
  }
}
