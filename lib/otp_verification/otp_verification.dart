import 'package:PinoyHelpHotline/contansts/color_constant.dart';
import 'package:PinoyHelpHotline/contansts/constant.dart';
import 'package:PinoyHelpHotline/contansts/screen_util.dart';
import 'package:PinoyHelpHotline/contansts/string.dart';
import 'package:PinoyHelpHotline/contansts/style_constant.dart';
import 'package:PinoyHelpHotline/loader/loader.dart';
import 'package:PinoyHelpHotline/util/platform_util.dart';
import 'package:PinoyHelpHotline/util/sharedpreference_util.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:logger/logger.dart';
import 'package:package_info/package_info.dart';
import 'package:sms_autofill/sms_autofill.dart';

class OtpVerification extends StatefulWidget {
  OtpVerification(
      {required this.phone,
      required this.verificationId,
      int? this.resendToken});

  final String phone, verificationId;
  final int? resendToken;

  @override
  OtpVerificationState createState() => OtpVerificationState();
}

class OtpVerificationState extends State<OtpVerification> {
  var otpCode = "";
  var logger = Logger();
  String verificationId = "";
  int? resendToken;
  var appVersion = "",
      osVersion = "",
      deviceType = "",
      deviceModel = "",
      deviceBrand = "";
  PackageInfo packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );
  FirebaseAuth auth = FirebaseAuth.instance;
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();

  @override
  void initState() {
    verificationId = widget.verificationId;
    resendToken = widget.resendToken;

    // TODO: implement initState
    super.initState();
    getDeviceInto();
    listenAutoSms();
  }

  listenAutoSms() async {
    final signature = await SmsAutoFill().getAppSignature;
    await SmsAutoFill().listenForCode;
  }

  @override
  void dispose() {
    SmsAutoFill().unregisterListener();
    super.dispose();
  }

  getDeviceInto() async {
    var deviceData;

    try {
      packageInfo = await PackageInfo.fromPlatform();
      appVersion = packageInfo.version;
      if (PlatformInfo().getCurrentPlatformType() == PlatformType.Android) {
        deviceData = await deviceInfoPlugin.androidInfo;
        deviceBrand = deviceData.brand;
        deviceType = "Android";
        deviceModel = deviceData.model;
        osVersion = deviceData.version.sdkInt.toString();
      } else if (PlatformInfo().getCurrentPlatformType() == PlatformType.iOS) {
        deviceType = "IOS";
      }
    } on PlatformException {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(children: [
        Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(
                child: Padding(
                  padding: const EdgeInsets.only(top: 50),
                  child: Image.asset(
                    'assets/images/otpscreen_icon.png',
                    scale: 1.5,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30),
                child: Text('Verification',
                    style: fontStyleSemiBold.copyWith(
                        color: Colors.black87, fontSize: 30)),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(25, 5, 25, 50),
                child: Text("You will get OTP on ${widget.phone}'",
                    style: fontStyleMedium.copyWith(
                      color: Colors.grey,
                      fontSize: 15,
                    )),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30, bottom: 25),
                child: Container(
                    //   margin: const EdgeInsets.only(top: 30, bottom: 25),
                    child: PinFieldAutoFill(
                        //controller: _otpController,
                        decoration: UnderlineDecoration(
                          textStyle: fontStyleNormal.copyWith(
                              fontSize: 20, color: Colors.black),
                          colorBuilder:
                              FixedColorBuilder(Colors.black.withOpacity(0.3)),
                        ),
                        //ration, BoxLooseDecoration or BoxTightDecoration see https://github.com/TinoGuo/pin_input_text_field for more info,
                        currentCode: otpCode,
                        // prefill with a code
                        onCodeSubmitted: (code) {
                          signInUser(code);
                        },
                        //code submitted callback
                        onCodeChanged: (code) {
                          otpCode = code!;
                          // //logger.e("OTP is ${code}");
                          if (code.length == 6) {
                            FocusScope.of(context).requestFocus(FocusNode());
                          }
                          setState(() {});
                        },
                        //code changed callback
                        codeLength: 6 //code length, default 6
                        )),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Container(
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () {
                        if (otpCode.length == 6) {
                          showLoader(true);
                          signInUser(otpCode);
                        }
                      },
                      child: Container(
                        alignment: Alignment.center,
                        height: buttonHeight,
                        child: Text(
                          "VERIFY",
                          textAlign: TextAlign.center,
                          style: fontStyleMedium.copyWith(
                              color: Colors.white, fontSize: 15),
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(6)),
                            color: (otpCode.length == 6)
                                ? ColorUtil.colorPrimary
                                : ColorUtil.colorPrimary.withOpacity(0.5)),
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      "Don't get OTP?",
                      style: fontStyleNormal.copyWith(
                        fontSize: 13,
                        color: Colors.grey,
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        showLoader(true);
                        sendOtp();
                      },
                      child: Container(
                        alignment: Alignment.center,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 5.0),
                          child: Text(
                            'Resend OTP',
                            style: fontStyleMedium.copyWith(
                              color: ColorUtil.colorPrimary,
                              fontSize: 13,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ]),
    );
  }

  signInUser(String code) async {
    // Create a PhoneAuthCredential with the code
    PhoneAuthCredential credential = PhoneAuthProvider.credential(
        verificationId: verificationId, smsCode: code);

    // Sign the user in (or link) with the credential
    auth
        .signInWithCredential(credential)
        .then((value) => {
              logger.e("Logged in user uuid ${value.user!.uid}"),
              checkUserExist(value.user!.uid)
            })
        .catchError((onError) =>
            {print(onError), showLoader(false), showToast(onError.toString())});
  }

  checkUserExist(String UUID) {
    CollectionReference users = FirebaseFirestore.instance
        .collection(ConstantUtil.FIREBASE_TABLE_USERS);

    logger.e("uuid is ${UUID}");
    users.doc(UUID).get().then((DocumentSnapshot documentSnapshot) {
      if (documentSnapshot.exists) {
        logger.e("User exist in firestore");
        SharedPreferencesUtil.setString(
            ConstantUtil.PREF_FNAME, documentSnapshot["fname"]);
        SharedPreferencesUtil.setString(
            ConstantUtil.PREF_LNAME, documentSnapshot["lname"]);
        SharedPreferencesUtil.setString(
            ConstantUtil.PREF_PROFILE, documentSnapshot["profile"]);
        SharedPreferencesUtil.setString(
            ConstantUtil.PREF_ROLE, documentSnapshot["role"]);

        SharedPreferencesUtil.setString(
            ConstantUtil.PREF_LOGIN_USER_UUID, UUID);
        SharedPreferencesUtil.setString(
            ConstantUtil.PREF_LOGIN_USER_PHONE, widget.phone);

        SharedPreferencesUtil.setBoolean(ConstantUtil.PREF_IS_FREE_USER,
            documentSnapshot[ConstantUtil.FIREBASE_IS_FREE_USER]);


        navigateToHome();
      } else
        addUserData(UUID);
    }).catchError((error) => {
          showLoader(false),
          print("Failed to update user: $error"),
          showToast(error)
        });
  }

  /**
   * UPDATE LOGGED IN USER DATA IN FIRE STORE
   */
  addUserData(String uuid) {
    logger.e("User not exist in firestore");
    Map<String, dynamic> userData = {
      ConstantUtil.FIREBASE_KEY_PHONE: widget.phone,
      ConstantUtil.FIREBASE_KEY_USER_UUID: uuid,
      ConstantUtil.FIREBASE_KEY_CREATED_AT: FieldValue.serverTimestamp(),
      // ConstantUtil.FIREBASE_KEY_FNAME: "",
      // ConstantUtil.FIREBASE_KEY_LNAME: "",
      // ConstantUtil.FIREBASE_KEY_PROFILE: "",
      // ConstantUtil.FIREBASE_KEY_GENDER: "",
      ConstantUtil.FIREBASE_KEY_APP_VERSION: appVersion,
      ConstantUtil.FIREBASE_KEY_OS_VERSION: osVersion,
      ConstantUtil.FIREBASE_KEY_DEVICE_TYPE: deviceType,
      ConstantUtil.FIREBASE_KEY_DEVICE_MODAL: deviceModel,
      ConstantUtil.FIREBASE_KEY_DEVICE_BRAND: deviceBrand,
      ConstantUtil.FIREBASE_KEY_STATUS: ConstantUtil.RECORD_ACTIVE,
      // ConstantUtil.FIREBASE_KEY_ROLE: ConstantUtil.ROLE_USER,
    };

    users
        .doc(uuid)
        .set(userData)
        .then((value) => {
              SharedPreferencesUtil.setString(
                  ConstantUtil.PREF_LOGIN_USER_UUID, uuid),
              SharedPreferencesUtil.setString(
                  ConstantUtil.PREF_LOGIN_USER_PHONE, widget.phone),
              // SharedPreferencesUtil.setString(
              //     ConstantUtil.PREF_ROLE, ConstantUtil.ROLE_USER),
              navigateToHome()
            })
        .catchError((onError) => {
              logger.e(onError),
              showLoader(false),
              showToast(onError.toString())
            });
  }

  showToast(String msg) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: ColorUtil.colorPrimary,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  sendOtp() async {
    await FirebaseAuth.instance.verifyPhoneNumber(
      phoneNumber: widget.phone,
      timeout: const Duration(seconds: 120),
      forceResendingToken: widget.resendToken,
      verificationCompleted: (PhoneAuthCredential credential) {
        auth
            .signInWithCredential(credential)
            .then((value) => {
                  logger.e("sendOtp Logged in user uuid ${value.user!.uid}"),
                  checkUserExist(value.user!.uid)
                })
            .catchError((onError) => {showToast(onError.toString())});
      },
      verificationFailed: (FirebaseAuthException e) {
        showLoader(false);
        if (e.code == ConstantUtil.FIREBASE_ERR_CODE_INVALID) {
          showToast(StringUtil.ERR_PHONE_INVALID);
        } else
          showToast(e.message!);
      },
      codeSent: (String verificationId, int? resendToken) {
        //showLoader(false);
        this.verificationId = verificationId;
        this.resendToken = resendToken;
        showToast(StringUtil.SUCC_OTP_SENT);
      },
      codeAutoRetrievalTimeout: (String verificationId) {
        showLoader(false);
        showToast(StringUtil.ERR_OTP_TIMEOUT);
      },
    );
  }

  navigateToHome() {
    Navigator.pushNamedAndRemoveUntil(
        context, ScreenUtil.HOME_SCREEN, (r) => false);
  }

  showLoader(bool show) {
    if (show)
      showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) => Loader());
    else
      Navigator.pop(context);
  }
}
