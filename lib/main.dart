import 'package:PinoyHelpHotline/screen/home/home_sceen.dart';
import 'package:PinoyHelpHotline/screen/splash/splash.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart' show kDebugMode;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_windowmanager/flutter_windowmanager.dart';
import 'package:overlay_support/overlay_support.dart';

import 'contansts/color_constant.dart';
import 'contansts/screen_util.dart';

/*
flutter run -t lib/main_dev.dart  --flavor=dev
# Debug signing configuration + dev flavor
flutter run -t lib/main_dev.dart  --debug --flavor=dev
flutter run -t lib/main_dev.dart  --release --flavor=dev
flutter build appbundle -t lib/main_dev.dart  --flavor=dev
flutter build apk -t lib/main_dev.dart  --flavor=dev
*/

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  // await FirebaseAuth.instance.useEmulator('http://localhost:9099');
  //await initFlavor();

  if (kDebugMode) {
    // Force disable Crashlytics collection while doing every day development.
    // Temporarily toggle this to true if you want to test crash reporting in your app.
    await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(false);
  } else {
    // Handle Crashlytics enabled status when not in Debug,
    // e.g. allow your users to opt-in to crash reporting.
    await FlutterWindowManager.addFlags(FlutterWindowManager.FLAG_SECURE);
  }
  runApp(PinoyHelpHotLine());
}

class PinoyHelpHotLine extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        //or set color with: Color(0xFF0000FF)
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark));
    return OverlaySupport(
      child: MaterialApp(
        title: "PinoyHelpHotLine",
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          // This is the theme of your application.

          primaryColor: ColorUtil.colorPrimary,
          unselectedWidgetColor: ColorUtil.colorSecondary,
        ),
        home: SplashScreen(),
        routes: <String, WidgetBuilder>{
          ScreenUtil.HOME_SCREEN: (BuildContext context) => new HomeScreen(),
        },
      ),
    );
  }
}
