import 'package:PinoyHelpHotline/contansts/color_constant.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';

class Loader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return DefaultTextStyle(
      style: TextStyle(decoration: TextDecoration.none),
      child: Center(
        child: JumpingDotsProgressIndicator(
          fontSize: 100.0,
          color: ColorUtil.colorSecondary,
        ),
      ),
    );
  }
}
