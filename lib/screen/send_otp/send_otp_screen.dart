import 'package:PinoyHelpHotline/contansts/color_constant.dart';
import 'package:PinoyHelpHotline/contansts/constant.dart';
import 'package:PinoyHelpHotline/contansts/screen_util.dart';
import 'package:PinoyHelpHotline/contansts/string.dart';
import 'package:PinoyHelpHotline/contansts/style_constant.dart';
import 'package:PinoyHelpHotline/loader/loader.dart';
import 'package:PinoyHelpHotline/otp_verification/otp_verification.dart';
import 'package:PinoyHelpHotline/util/drawer_menu.dart';
import 'package:PinoyHelpHotline/util/sharedpreference_util.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:logger/logger.dart';

class SendOtpScreen extends StatefulWidget {
  @override
  SendOtpScreenState createState() => SendOtpScreenState();
}

class SendOtpScreenState extends State<SendOtpScreen> {
  final _phoneController = TextEditingController();
  var otpCode = "";
  var logger = Logger();

  String verificationId = "";
  int? resendToken;
  FirebaseAuth auth = FirebaseAuth.instance;

  CollectionReference users = FirebaseFirestore.instance.collection('users');

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 50),
                    child: Image.asset(
                      'assets/images/otpscreen_icon.png',
                      scale: 1.5,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 30),
                  child: Text('Verification',
                      style: TextStyle(
                        color: Colors.black87,
                        fontSize: 25,
                        fontWeight: FontWeight.w900,
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(25, 5, 25, 50),
                  child: Text(
                      "We will send on you a ONE TIME PASSWORD on your mobile number ",
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                      )),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: ColorUtil.colorPrimary),
                      borderRadius: BorderRadius.all(
                        new Radius.circular(8),
                      ),
                    ),
                    child: TextField(
                      inputFormatters: [LengthLimitingTextInputFormatter(10)],
                      controller: _phoneController,
                      textAlignVertical: TextAlignVertical.center,
                      style: styleLoginInput,
                      onChanged: (text) {
                        if (_phoneController.text.length == 10)
                          FocusScope.of(context).requestFocus(FocusNode());
                        setState(() {});
                      },
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.phone,
                          color: ColorUtil.colorPrimary,
                        ),
                        border: InputBorder.none,
                        hintText: 'Enter phone with country code',
                        hintStyle: styleInputHint,
                      ),
                    )),
                Container(
                  padding: const EdgeInsets.only(top: 20),
                  child: new GestureDetector(
                      onTap: () {},
                      child: Container(
                        height: buttonHeight,
                        child: new Material(
                          child: new InkWell(
                            onTap: () {
                              if (_phoneController.text.length == 10) {
                                showLoader(true);
                                sendOtp();
                              }
                            },
                            child: Container(
                              /*      decoration: BoxDecoration(
                                color: ColorUtil.colorPrimary,
                                borderRadius: BorderRadius.circular(6),
                                //border corner radius
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 2,
                                    blurRadius: 7,
                                    offset: Offset(
                                        0, 2), // changes position of shadow
                                  ),
                                  //you can set more BoxShadow() here
                                ],
                              ),*/
                              child: Text(
                                'GET OTP',
                                style: styleButton,
                              ),
                              alignment: Alignment.center,
                            ),
                          ),
                          color: Colors.transparent,
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(6)),
                            color: (_phoneController.text.length == 10)
                                ? ColorUtil.colorPrimary
                                : ColorUtil.colorPrimary.withOpacity(0.5)),
                      )),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  sendOtp() async {
    await FirebaseAuth.instance.verifyPhoneNumber(
      phoneNumber: _phoneController.text,
      timeout: const Duration(seconds: 120),
      verificationCompleted: (PhoneAuthCredential credential) {
        auth
            .signInWithCredential(credential)
            .then((value) => {
                  showLoader(false),
                  navigateToHome(value.user!.uid),
                })
            .catchError((onError) => {showToast(onError.toString())});
      },
      verificationFailed: (FirebaseAuthException e) {
        showLoader(false);
        if (e.code == ConstantUtil.FIREBASE_ERR_CODE_INVALID) {
          showToast(StringUtil.ERR_PHONE_INVALID);
        } else
          showToast(e.message!);
      },
      codeSent: (String verificationId, int? resendToken) {
        showLoader(false);
        showToast(StringUtil.SUCC_OTP_SENT);

        Navigator.push(
          context,
          CupertinoPageRoute(
            builder: (context) => OtpVerification(
                phone: _phoneController.text,
                verificationId: verificationId,
                resendToken: resendToken),
          ),
        );
      },
      codeAutoRetrievalTimeout: (String verificationId) {},
    );
  }

  /**
   * NAVIGATE USER TO HOME SCREEN
   */
  navigateToHome(String UUID) {
    CollectionReference users = FirebaseFirestore.instance
        .collection(ConstantUtil.FIREBASE_TABLE_USERS);

    users.doc(UUID).get().then((DocumentSnapshot documentSnapshot) {
      showLoader(false);
      if (documentSnapshot.exists) {
        // SharedPreferencesUtil.setString(
        //     ConstantUtil.PREF_FNAME, documentSnapshot["fname"]);
        // SharedPreferencesUtil.setString(
        //     ConstantUtil.PREF_LNAME, documentSnapshot["lname"]);
        // SharedPreferencesUtil.setString(
        //     ConstantUtil.PREF_PROFILE, documentSnapshot["profile"]);
      }
      Navigator.pushNamedAndRemoveUntil(
          context, ScreenUtil.BASE_SCREEN, (r) => false);
    }).catchError((error) => {
          showLoader(false),
          print("Failed to update user: $error"),
          showToast(error)
        });
  }

  showLoader(bool show) {
    if (show)
      showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) => Loader());
    else
      Navigator.pop(context);
  }
}
