import 'package:PinoyHelpHotline/contansts/color_constant.dart';
import 'package:PinoyHelpHotline/contansts/constant.dart';
import 'package:PinoyHelpHotline/contansts/style_constant.dart';
import 'package:PinoyHelpHotline/util/sharedpreference_util.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:photo_view/photo_view.dart';
import 'package:url_launcher/url_launcher.dart';

class PostDetailScreen extends StatefulWidget {
  // String des, title, url, actualPrice, phone, expectedPrice, model;
  DocumentSnapshot snapshot;

  PostDetailScreen({required this.snapshot});

  @override
  PostDetailScreenState createState() => PostDetailScreenState();
}

class PostDetailScreenState extends State<PostDetailScreen>
    with WidgetsBindingObserver {
  String phone = "";
  List images = [];

  @override
  void initState() {
    super.initState();
    images.addAll(widget.snapshot["images"]);
    getUserDetail();
  }

  getUserDetail() {
    FirebaseFirestore.instance
        .collection('users')
        .doc(widget.snapshot["userId"])
        .get()
        .then((DocumentSnapshot documentSnapshot) {
      if (documentSnapshot.exists) {
        print('Document data: ${documentSnapshot.data()}');
        phone = documentSnapshot["phone"];
        setState(() {});
      } else {
        print('Document does not exist on the database');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: ColorUtil.colorWhite,
        child: Column(
          children: [
            Stack(children: [
              Container(
                child: CarouselSlider(
                    items: images
                        .map(
                          (item) => ClipRRect(
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(35.0)),
                              child: CachedNetworkImage(
                                imageUrl: item,
                                fit: BoxFit.cover,
                                width: double.infinity,
                                progressIndicatorBuilder:
                                    (context, url, downloadProgress) => Center(
                                  child: SizedBox(
                                    width: 30.0,
                                    height: 30.0,
                                    child: CircularProgressIndicator(
                                      value: downloadProgress.progress,
                                      color: ColorUtil.colorPrimary,
                                    ),
                                  ),
                                ),
                                errorWidget: (context, url, error) => Icon(
                                    Icons.error,
                                    color: ColorUtil.colorPrimary),
                              )),
                        )
                        .toList(),
                    options: CarouselOptions(
                      height: 350,
                      aspectRatio: 16 / 9,
                      viewportFraction: 1,
                      initialPage: 0,
                      enableInfiniteScroll: true,
                      reverse: false,
                      autoPlay: true,
                      autoPlayInterval: Duration(seconds: 3),
                      autoPlayAnimationDuration: Duration(milliseconds: 800),
                      autoPlayCurve: Curves.fastOutSlowIn,
                      enlargeCenterPage: true,
                      scrollDirection: Axis.horizontal,
                    )),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Padding(
                      padding: EdgeInsets.only(top: 40, left: 10),
                      child: Icon(
                        Icons.arrow_back_ios_outlined,
                        color: Colors.black87,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 40, right: 20),
                    child: Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          color: Colors.white, shape: BoxShape.circle),
                      child: InkWell(
                        onTap: () {
                          checkFavItem(widget.snapshot["id"]);
                        },
                        child: Icon(
                          Icons.favorite_border,
                          color: Colors.black87,
                          size: 20,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ]),
            Expanded(
                child: Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.only(right: 10, left: 10, top: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(widget.snapshot["title"], style: styleDetailTitle),
                        Text(widget.snapshot["expectedPrice"],
                            style: styleExpPriceTitle),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0, top: 15),
                    child: Text(
                      "PRODUCT DETAILS",
                      style: styleProductDetailText,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0, top: 5),
                    child: Text(
                      widget.snapshot["description"],
                      style: styleDescription,
                    ),
                  ),
                ],
              ),
            )),
            InkWell(
              onTap: () {
                launch("tel://${phone}");
              },
              child: Align(
                alignment: Alignment.bottomRight,
                child: Container(
                  width: 180,
                  padding: EdgeInsets.all(12),
                  decoration: BoxDecoration(
                    color: ColorUtil.colorPrimary,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(45),
                    ),
                  ),
                  child: Center(
                    child: Text(
                      'Call Now',
                      style: styleCallNow,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  getImageViewFromUrl() {
    return Hero(
        tag: "Image",
        child: Stack(
          children: [
            PhotoView(
                backgroundDecoration: BoxDecoration(color: Colors.transparent),
                imageProvider: NetworkImage(widget.snapshot["url"])),
            Positioned(
              top: 15,
              right: 10,
              child: InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Icon(
                  Icons.close,
                  color: Colors.red,
                  size: 30.0,
                ),
              ),
            ),
          ],
        ));
  }

  showPaymentAlert(BuildContext context) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.transparent,
            content: getImageViewFromUrl(),
          );
        });
  }

  checkFavItem(String id) {
    List<String> temp =
        SharedPreferencesUtil.getStringList(ConstantUtil.PREF_FAV_ITEMS);
    var index = temp.indexOf(id);

    if (index == -1)
      temp.add(id);
    else
      temp.removeAt(index);

    SharedPreferencesUtil.setStringList(ConstantUtil.PREF_FAV_ITEMS, temp);

    setState(() {});
  }
}
