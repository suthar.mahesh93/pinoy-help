import 'dart:async';

import 'package:PinoyHelpHotline/contansts/color_constant.dart';
import 'package:PinoyHelpHotline/contansts/style_constant.dart';
import 'package:PinoyHelpHotline/screen/home/home_sceen.dart';
import 'package:PinoyHelpHotline/util/sharedpreference_util.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';

class SplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  FirebaseAuth auth = FirebaseAuth.instance;
  var logger = Logger();

  @override
  void initState() {
    super.initState();
    init();
  }

  @override
  void dispose() {
    super.dispose();
  }

  init() async {
    await SharedPreferencesUtil.init();
    // if (auth.currentUser == null)
    //   Timer(
    //       Duration(seconds: 3),
    //       () => {
    //             Navigator.pushReplacement(context,
    //                 MaterialPageRoute(builder: (context) => SendOtpScreen()))
    //           });
    // else
    Timer(
        Duration(seconds: 3),
        () => {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => HomeScreen()))
            });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      decoration: BoxDecoration(
          image: DecorationImage(
        image: AssetImage("assets/images/splash.png"),
        fit: BoxFit.cover,
      )),
      child: Stack(
        children: [
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/images/logo.jpg',
                  height: 150,
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(bottom: 5, right: 5),
            alignment: Alignment.bottomCenter,
            child: Row(
              children: [
                Text(
                  "Developed by ",
                  style: fontStyleNormal.copyWith(
                      fontSize: 12, color: Colors.grey),
                ),
                Text(
                  "Kreative TechNest",
                  style: fontStyleNormal.copyWith(
                      fontSize: 12, color: ColorUtil.colorSecondary),
                ),
              ],
            ),
          )
        ],
      ),
    ));
  }
}
