import 'dart:core';
import 'dart:io';

import 'package:PinoyHelpHotline/contansts/color_constant.dart';
import 'package:PinoyHelpHotline/contansts/constant.dart';
import 'package:PinoyHelpHotline/contansts/string.dart';
import 'package:PinoyHelpHotline/contansts/style_constant.dart';
import 'package:PinoyHelpHotline/custom/custom_dropdown.dart';
import 'package:PinoyHelpHotline/custom/custom_textInput.dart';
import 'package:PinoyHelpHotline/custom/custom_textInput_multiline.dart';
import 'package:PinoyHelpHotline/loader/loader.dart';
import 'package:PinoyHelpHotline/model/currency.dart';
import 'package:PinoyHelpHotline/util/menu_items.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:logger/logger.dart';

class AddPostScreen extends StatefulWidget {
  @override
  AddPostScreenState createState() => AddPostScreenState();
}

class AddPostScreenState extends State<AddPostScreen> {
  var firebaseStorage = firebase_storage.FirebaseStorage.instance;
  List<String> urls = [];
  int currentImageIndex = 0;
  List<DropdownMenuItem<String>> _statusDropdownList = [];
  List<DropdownMenuItem<CurrencyItem>> _currencyDropdownList = [];
  final List<String> _statusList = [
    'Sold',
    'Unsold',
  ];
  final List<String> uploadedImages = [];
  CollectionReference users =
      FirebaseFirestore.instance.collection(ConstantUtil.FIREBASE_TABLE_USERS);
  String _selectedType = "";
  final _titleController = TextEditingController();
  final _phoneNumberController = TextEditingController();
  final _actualPriceController = TextEditingController();
  final _exceptedPriceController = TextEditingController();
  final _descriptionController = TextEditingController();
  var _logger = Logger();
  late String popop;
  FirebaseAuth auth = FirebaseAuth.instance;
  late CurrencyItem currencyItem;
  List<CurrencyItem> currencyList = getCurrency();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _statusDropdownList = getDropdownList(_statusList);
    _selectedType = _statusList[0];

    _currencyDropdownList = getDropdownCurrencyList(currencyList);
    print("-----length is ${_currencyDropdownList.length}");
    currencyItem = currencyList[0];
  }

  final CarouselController _buttonCarouselController = CarouselController();

  late List<Widget> imageSliders = <Widget>[];

  final picker = ImagePicker();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorUtil.colorWhite,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: ColorUtil.colorPrimary,
        title: Text("Add Post", style: styleTitle),
        elevation: 3,
        automaticallyImplyLeading: false,
      ),
      body: ListView(
        children: [
          Stack(children: [
            uploadedImages.length > 0
                ? CarouselSlider(
                    items: uploadedImages
                        .map(
                          (item) => ClipRRect(
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(35.0)),
                              child: CachedNetworkImage(
                                imageUrl: item,
                                fit: BoxFit.cover,
                                width: double.infinity,
                                progressIndicatorBuilder:
                                    (context, url, downloadProgress) => Center(
                                  child: SizedBox(
                                    width: 30.0,
                                    height: 30.0,
                                    child: CircularProgressIndicator(
                                      value: downloadProgress.progress,
                                      color: ColorUtil.colorPrimary,
                                    ),
                                  ),
                                ),
                                errorWidget: (context, url, error) => Icon(
                                    Icons.error,
                                    color: ColorUtil.colorPrimary),
                              )),
                        )
                        .toList(),
                    options: CarouselOptions(
                      height: 220,
                      aspectRatio: 10 / 3,
                      viewportFraction: 1,
                      initialPage: 0,
                      enableInfiniteScroll: true,
                      reverse: false,
                      autoPlay: false,
                      autoPlayInterval: Duration(seconds: 3),
                      autoPlayAnimationDuration: Duration(milliseconds: 800),
                      autoPlayCurve: Curves.fastOutSlowIn,
                      enlargeCenterPage: true,
                      scrollDirection: Axis.horizontal,
                      onPageChanged: (index, reason) {
                        currentImageIndex = index;
                      },
                    ))
                : new Container(
                    height: 220,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.only(bottomLeft: Radius.circular(35.0)),
                      border:
                          Border.all(color: ColorUtil.colorPrimary, width: .5),
                    ),
                    child: Center(
                      child: Image.asset(
                        'assets/images/place_holder.png',
                        height: 100,
                        width: 120,
                        color: ColorUtil.colorPrimary,
                      ),
                    ),
                  ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Visibility(
                  visible: uploadedImages.length < 4,
                  child: InkWell(
                    onTap: () {
                      getImage();
                    },
                    child: Padding(
                      padding: EdgeInsets.only(top: 20, left: 20),
                      child: Container(
                        width: 28.0,
                        height: 28.0,
                        decoration: BoxDecoration(
                            color: Colors.white, shape: BoxShape.circle),
                        child: Icon(
                          Icons.add,
                          color: Colors.black87,
                          size: 25,
                        ),
                      ),
                    ),
                  ),
                ),
                Visibility(
                  visible: uploadedImages.length > 0,
                  child: InkWell(
                    onTap: () {
                      if (uploadedImages.length > 0) showDeleteAlert(context);
                    },
                    child: Padding(
                      padding: EdgeInsets.only(top: 20, right: 20),
                      child: Container(
                        width: 30.0,
                        height: 30.0,
                        decoration: BoxDecoration(
                            color: Colors.white, shape: BoxShape.circle),
                        child: Icon(
                          Icons.delete,
                          color: Colors.black87,
                          size: 22,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            )
          ]),
          Padding(
            padding: const EdgeInsets.fromLTRB(15, 15, 15, 15),
            child: Column(
              children: <Widget>[
                CustomTextInput(
                    hintTextString: "Title",
                    textEditController: _titleController,
                    inputType: InputType.Default,
                    readOnly: false),
                CustomTextInput(
                    hintTextString: "Price",
                    textEditController: _exceptedPriceController,
                    inputType: InputType.Number,
                    readOnly: false),
                CustomDropdown(
                  dropdownMenuItemList: _statusDropdownList,
                  onChanged: _onChangeTypeDropdown,
                  value: _selectedType,
                  isEnabled: true,
                  readOnly: false,
                ),
                CustomDropdown(
                  dropdownMenuItemList: _currencyDropdownList,
                  onChanged: _onChangeCurrencyDropdown,
                  value: currencyItem,
                  isEnabled: true,
                  readOnly: false,
                ),
                CustomTextInputMultiLine(
                  hintTextString: "Description",
                  textEditController: _descriptionController,
                  maxLines: 8,
                  readOnly: false,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    new Container(
                        margin: const EdgeInsets.only(
                            top: 40, bottom: 40, right: 5),
                        padding: EdgeInsets.only(
                            top: 15, left: 50, right: 50, bottom: 15),
                        height: buttonHeight,
                        child: new Material(
                          child: new InkWell(
                            onTap: () {
                              if (isValidDetails()) {
                                showLoader(true);
                                addPostData();
                              }
                            },
                            child: Center(
                              child: Text(
                                "DELETE",
                                textAlign: TextAlign.center,
                                style: styleButton,
                              ),
                            ),
                          ),
                          color: Colors.transparent,
                        ),
                        decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(100)),
                            color: ColorUtil.colorPrimary)),
                    new Container(
                        margin: const EdgeInsets.only(top: 40, bottom: 40),
                        padding: EdgeInsets.only(
                            top: 15, left: 50, right: 50, bottom: 15),
                        height: buttonHeight,
                        child: new Material(
                          child: new InkWell(
                            onTap: () {
                              if (isValidDetails()) {
                                showLoader(true);
                                addPostData();
                              }
                            },
                            child: Center(
                              child: Text(
                                "ADD POST",
                                textAlign: TextAlign.center,
                                style: styleButton,
                              ),
                            ),
                          ),
                          color: Colors.transparent,
                        ),
                        decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(100)),
                            color: ColorUtil.colorPrimary)),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  showDeleteAlert(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          title: Icon(
            Icons.delete,
            color: ColorUtil.colorPrimary,
            size: 30,
          ),
          content: Text(
            'Are you sure to delete this item?',
            textAlign: TextAlign.center,
            style: styleAlertTitle,
          ),
          actions: <Widget>[
            Container(
              height: 0.3,
              color: const Color(0xFF707070),
              child: SizedBox(
                width: 500,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(top: 15),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        'NO',
                        style: styleAlertYes,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    color: Colors.white,
                    height: 50,
                  ),
                ),
                Container(
                  height: 40,
                  color: const Color(0xFF707070),
                  child: SizedBox(
                    width: 0.5,
                  ),
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(top: 15),
                    child: GestureDetector(
                      onTap: () {
                        deleteImage();
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        'YES',
                        style: styleAlertNo,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    color: Colors.white,
                    height: 50,
                  ),
                ),
              ],
            ),
          ],
        );
      },
    );
  }

  _onChangeTypeDropdown(String? currencyItem) {
    setState(() {
      _selectedType = currencyItem!;
    });
  }

  _onChangeCurrencyDropdown(CurrencyItem? currencyItem) {
    setState(() {
      this.currencyItem = currencyItem!;
    });
  }

  showLoader(bool show) {
    if (show)
      showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) => Loader());
    else
      Navigator.pop(context);
  }

  bool isValidDetails() {
    if (_titleController.text.isEmpty) {
      showToast(StringUtil.ERR_MISSING_FIELDS);
      return false;
    } else if (_phoneNumberController.text.isEmpty) {
      showToast(StringUtil.ERR_MISSING_PHONE);
      return false;
    } else if (_actualPriceController.text.isEmpty) {
      showToast(StringUtil.ERR_MISSING_ACTUAL_PRICE);
      return false;
    } else if (_exceptedPriceController.text.isEmpty) {
      showToast(StringUtil.ERR_MISSING_EXCEPTED_PRICE);
      return false;
    } else if (_descriptionController.text.isEmpty) {
      showToast(StringUtil.ERR_MISSING_DESCRIPTION);
      return false;
    }

/*    else if (_url.isEmpty) {
      showToast(StringUtil.ERR_MISSING_POST_URL);
      return false;
    }*/
    return true;
  }

  showToast(String msg) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: ColorUtil.colorPrimary,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  addPostData() {
    CollectionReference posts = FirebaseFirestore.instance
        .collection(ConstantUtil.FIREBASE_TABLE_POSTS);
    Map<String, dynamic> targetData = {
      ConstantUtil.FIREBASE_KEY_TITLE: _titleController.text,
      ConstantUtil.FIREBASE_KEY_PRICE: _actualPriceController.text,
      ConstantUtil.FIREBASE_KEY_DESCRIPTION: _descriptionController.text,
      ConstantUtil.FIREBASE_KEY_CURRENCY: currencyItem.currency,
      ConstantUtil.FIREBASE_KEY_CURRENCY_CODE: currencyItem.code,
      ConstantUtil.FIREBASE_KEY_TYPE: _selectedType.toString(),
      ConstantUtil.FIREBASE_KEY_URL: uploadedImages,
      ConstantUtil.FIREBASE_KEY_UPDATED_AT: FieldValue.serverTimestamp(),
      ConstantUtil.FIREBASE_KEY_CREATED_AT: FieldValue.serverTimestamp(),
      ConstantUtil.FIREBASE_KEY_STATUS: ConstantUtil.RECORD_ACTIVE,
      ConstantUtil.FIREBASE_KEY_USER_UUID: auth.currentUser!.uid,
    };
    posts
        .add(targetData)
        .then((value) async => {
              await posts.doc().update({'postId': value.id}),
              clearData(),
              showToast("Post created successfully.")
            })
        .catchError((error) => {
              showLoader(false),
              print("Failed to update post: $error"),
              showToast(error)
            });
  }

  clearData() {
    _phoneNumberController.text = "";
    _actualPriceController.text = "";
    _titleController.text = "";
    _exceptedPriceController.text = "";
    _descriptionController.text = "";

    setState(() {});
  }

  sendAddPostNotification() {
    _logger.d("sendAddPostNotification");
    String title = "", body = "";

    title = "New ${_selectedType.toString()} is added.";
    body = "Please check ${_selectedType.toString()} section for more details.";

    //sendPushNotification(title, body, ConstantUtil.NOTIFICATION_TYPE_ADD_POST);
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    if (pickedFile != null) _cropImage(pickedFile.path);
  }

  _cropImage(String? pathFile) async {
    File? croppedFile = await ImageCropper.cropImage(
        sourcePath: pathFile!,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
          // CropAspectRatioPreset.ratio3x2,
          // CropAspectRatioPreset.original,
          // CropAspectRatioPreset.ratio4x3,
          // CropAspectRatioPreset.ratio16x9
        ],
        aspectRatio: CropAspectRatio(ratioX: 1, ratioY: 1),
        maxHeight: 300,
        maxWidth: 300,
        //compressQuality:40,
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Crop Photo',
            toolbarColor: ColorUtil.colorSecondary,
            toolbarWidgetColor: ColorUtil.colorItem,
            initAspectRatio: CropAspectRatioPreset.square,
            activeControlsWidgetColor: ColorUtil.colorSecondary,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          minimumAspectRatio: 1.0,
        ));

    try {
      if (croppedFile != null) {
        print("---------");
        showLoader(true);
        String url = "user/+${auth.currentUser!.uid}";
        print("Bucket url is ${url}");
        // also tried result.files.single.path = null
        await firebaseStorage.ref(url).putFile(croppedFile);
        String downloadURL = await firebaseStorage.ref(url).getDownloadURL();
        uploadedImages.add(downloadURL);
        _logger.e("URL is $downloadURL");
        showLoader(false);
        setState(() {});
      }
    } catch (e) {
      showLoader(false);
    }
  }

  Future deleteImage() async {
    //setState(() => imgList.removeAt(index));
  }
}

class ImageConfig {
  String source;
  String path;

  ImageConfig({required this.source, required this.path});
}
