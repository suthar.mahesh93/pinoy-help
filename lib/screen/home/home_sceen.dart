import 'package:PinoyHelpHotline/contansts/color_constant.dart';
import 'package:PinoyHelpHotline/contansts/style_constant.dart';
import 'package:PinoyHelpHotline/screen/add_post/add_post_screen.dart';
import 'package:PinoyHelpHotline/screen/item_list/item_list_screen.dart';
import 'package:PinoyHelpHotline/screen/send_otp/send_otp_screen.dart';
import 'package:PinoyHelpHotline/screen/wishlist/wishlist_screen.dart';
import 'package:dot_navigation_bar/dot_navigation_bar.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';

import '../account/account_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  late BuildContext myContext;
  int _pageIndex = 0;

  String appbarTitleString = "Pinoy Help Hotline";
  var appBarTitleText = "Pinoy Help Hotline";
  List<Widget> tabPages = [
    ItemListScreen(),
    WishListScreen(),
    AddPostScreen(),
    AccountScreen(),
  ];

  var logger = Logger();
  var bseData, nseData;
  FirebaseAuth auth = FirebaseAuth.instance;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      // backgroundColor: Colors.grey.shade200,
      body: Center(
        child: tabPages.elementAt(_pageIndex),
      ),
      extendBody: true,
      bottomNavigationBar: DotNavigationBar(
        backgroundColor: ColorUtil.colorPrimary,
        margin: EdgeInsets.only(left: 10, right: 10),
        currentIndex: _pageIndex,
        marginR: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
        dotIndicatorColor: ColorUtil.colorPrimary,
        unselectedItemColor: Colors.white,
        enableFloatingNavBar: true,
        onTap: onPageChanged,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 3,
            blurRadius: 6,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
        //itemPadding: EdgeInsets.zero,
        paddingR: EdgeInsets.only(top: 5, bottom: 5),
        items: [
          /// Home
          DotNavigationBarItem(
            icon: Icon(Icons.home_outlined),
            selectedColor: ColorUtil.colorSecondary,
          ),

          /// Likes
          DotNavigationBarItem(
            icon: Icon(Icons.favorite_border),
            selectedColor: ColorUtil.colorSecondary,
          ),

          /// Search
          DotNavigationBarItem(
            icon: Icon(Icons.add_box_outlined),
            selectedColor: ColorUtil.colorSecondary,
          ),

          /// Profile
          DotNavigationBarItem(
            icon: Icon(Icons.person_outline),
            selectedColor: ColorUtil.colorSecondary,
          ),
        ],
      ),
    );
  }

  void onPageChanged(int index) {
    // if (index == 2 && auth.currentUser == null)
    //   showLoginAlert();
    // else
      setState(() {
        _pageIndex = index;
      });
  }

  void showLoginAlert() {
    showDialog(
      barrierDismissible: false,
      barrierColor: Colors.black.withOpacity(0.6),
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          title: Icon(
            Icons.login_rounded,
            size: 50,
            color: ColorUtil.colorPrimary,
          ),
          content: Text(
            'Please login first to use this service.',
            textAlign: TextAlign.center,
            style: TextStyle(color: const Color(0xFF707070)),
          ),
          actions: <Widget>[
            Container(
              height: 0.3,
              color: const Color(0xFF707070),
              child: SizedBox(
                width: 700,
              ),
            ),
            Container(
              alignment: Alignment.center,
              color: Colors.white,
              height: 50,
              child: GestureDetector(
                onTap: () {
                  //Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SendOtpScreen()),
                  );
                },
                child: Text(
                  'Login',
                  style: styleAlertNo,
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
