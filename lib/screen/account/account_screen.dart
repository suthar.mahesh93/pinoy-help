import 'dart:core';
import 'dart:io';

import 'package:PinoyHelpHotline/contansts/color_constant.dart';
import 'package:PinoyHelpHotline/contansts/constant.dart';
import 'package:PinoyHelpHotline/contansts/style_constant.dart';
import 'package:PinoyHelpHotline/loader/loader.dart';
import 'package:PinoyHelpHotline/screen/my_ads/my_ads_screen.dart';
import 'package:PinoyHelpHotline/screen/send_otp/send_otp_screen.dart';
import 'package:PinoyHelpHotline/util/drawer_menu.dart';
import 'package:PinoyHelpHotline/util/sharedpreference_util.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:logger/logger.dart';
import 'package:url_launcher/url_launcher.dart';

class AccountScreen extends StatefulWidget {
  @override
  AccountScreenState createState() => AccountScreenState();
}

class AccountScreenState extends State<AccountScreen> {
  String _selectedGender = "", _url = "", currentUserUUID = "";
  CollectionReference posts =
      FirebaseFirestore.instance.collection(ConstantUtil.FIREBASE_TABLE_POSTS);
  var firebaseStorage = firebase_storage.FirebaseStorage.instance;
  FirebaseAuth auth = FirebaseAuth.instance;
  String phone = "";

  var _logger = Logger();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (auth.currentUser != null) phone = auth.currentUser!.phoneNumber!;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorUtil.colorWhite,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: ColorUtil.colorPrimary,
        title: Text("Account",
            style: fontStyleNormal.copyWith(
                fontSize: 24, color: ColorUtil.colorWhite)),
        elevation: 3,
        automaticallyImplyLeading: false,
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 20.0, bottom: 10.0),
                  child: InkWell(
                    onTap: () {
                      _pickImageAndUpload();
                    },
                    child: getProfileWidget(),
                  ),
                ),
                Text(
                  phone,
                  style: fontStyleSemiBold.copyWith(
                    fontSize: 15,
                    color: Colors.grey,
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, bottom: 20, left: 20),
            child: Column(
              children: [
                InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MyAdsScreen()),
                    );
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Icon(
                        Icons.document_scanner_rounded,
                        color: Colors.black87,
                        size: 22,
                      ),
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 8.0),
                              child: Text(
                                "My Ads",
                                style: fontStyleNormal.copyWith(
                                    fontSize: 18, color: Colors.black),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 8.0),
                              child: Icon(Icons.arrow_forward_ios_rounded),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: Divider(
                    height: 1,
                    color: Colors.grey,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: InkWell(
                    onTap: () {
                      rating();
                    },
                    child: Row(
                      children: [
                        Icon(
                          Icons.star,
                          color: Colors.black87,
                          size: 22,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text(
                            "Rate us",
                            style: fontStyleNormal.copyWith(
                                fontSize: 18, color: Colors.black),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: Divider(
                    height: 1,
                    color: Colors.grey,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: InkWell(
                    onTap: () {
                      launchEmail();
                    },
                    child: Row(
                      children: [
                        Icon(
                          Icons.mail_outline,
                          color: Colors.black87,
                          size: 22,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text(
                            "Contact us",
                            style: fontStyleNormal.copyWith(
                                fontSize: 18, color: Colors.black),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text(
                            "(pinoyhelphotline@gmail.com)",
                            style: fontStyleMedium.copyWith(
                                fontSize: 14, color: Colors.grey),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: Divider(
                    height: 1,
                    color: Colors.grey,
                  ),
                ),
                InkWell(
                  onTap: () {
                    if (auth.currentUser == null)
                      showLoginAlert(context);
                    else
                      showLogoutAlert(context);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: Row(
                      children: [
                        Icon(
                          Icons.login,
                          color: Colors.black87,
                          size: 22,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text(
                            auth.currentUser == null ? "Login" : "Logout",
                            style: fontStyleNormal.copyWith(
                                fontSize: 18, color: Colors.black),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: Divider(
                    height: 1,
                    color: Colors.grey,
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 30),
            child: Column(
              children: [Text("APP VERSION"), Text("0.0.1")],
            ),
          )
        ],
      ),
    );
  }

  getUserData() {
    showLoader(true);
    CollectionReference users = FirebaseFirestore.instance
        .collection(ConstantUtil.FIREBASE_TABLE_USERS);
    users.doc(currentUserUUID).get().then((DocumentSnapshot documentSnapshot) {
      showLoader(false);
      if (documentSnapshot.exists) {
        _url = documentSnapshot["profile"];
        if (documentSnapshot["gen"] != "")
          _selectedGender = documentSnapshot["gen"];
        _logger.d("Role is ${documentSnapshot["role"]}");
        _logger.d(
            "Is free user ${documentSnapshot[ConstantUtil.FIREBASE_IS_FREE_USER]}");
        SharedPreferencesUtil.setString(
            ConstantUtil.PREF_ROLE, documentSnapshot["role"]);

        SharedPreferencesUtil.setBoolean(ConstantUtil.PREF_IS_FREE_USER,
            documentSnapshot[ConstantUtil.FIREBASE_IS_FREE_USER]);

        setState(() {});
      }
    }).catchError((error) => {
          showLoader(false),
          print("Failed to update user: $error"),
          showToast(error)
        });
  }

  getProfileWidget() {
    if (_url.isEmpty)
      return Container(
        height: 100,
        width: 100,
        child: Center(
          child: Icon(
            Icons.person_rounded,
            color: Colors.black87,
            size: 80,
          ),
        ),
        decoration:
            BoxDecoration(color: Colors.grey.shade400, shape: BoxShape.circle),
      );
    else
      return Container(
        decoration: BoxDecoration(
            border: Border.all(color: ColorUtil.colorSecondary),
            borderRadius: BorderRadius.circular(100)),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(100),
            child: Image.network(
              _url,
              height: 100,
              fit: BoxFit.cover,
              width: 100,
            )),
      );
  }

  void rating() {
    launch("mailto:" "?subject=TestEmail&body=How are you%20");
  }

  void launchEmail() {
    launch("mailto:" "?subject=TestEmail&body=How are you%20");
  }

  _pickImageAndUpload() async {
    FilePickerResult? result = await FilePicker.platform
        .pickFiles(type: FileType.custom, allowedExtensions: ['jpg', 'png']);
    if (result != null) _cropImage(result.files.single.path);
  }

  _cropImage(String? pathFile) async {
    File? croppedFile = await ImageCropper.cropImage(
        sourcePath: pathFile!,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
          // CropAspectRatioPreset.ratio3x2,
          // CropAspectRatioPreset.original,
          // CropAspectRatioPreset.ratio4x3,
          // CropAspectRatioPreset.ratio16x9
        ],
        aspectRatio: CropAspectRatio(ratioX: 1, ratioY: 1),
        maxHeight: 300,
        maxWidth: 300,
        //compressQuality:40,
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Crop Photo',
            toolbarColor: ColorUtil.colorSecondary,
            toolbarWidgetColor: ColorUtil.colorItem,
            initAspectRatio: CropAspectRatioPreset.square,
            activeControlsWidgetColor: ColorUtil.colorSecondary,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          minimumAspectRatio: 1.0,
        ));
    try {
      if (croppedFile != null) {
        print("---------");
        showLoader(true);
        String url =
            "profile/+${SharedPreferencesUtil.getString(ConstantUtil.PREF_LOGIN_USER_UUID)}";
        print("Image url" + url);
        await firebaseStorage.ref(url).putFile(croppedFile);
        String downloadURL = await firebaseStorage.ref(url).getDownloadURL();
        _url = downloadURL;
        _logger.e("URL is $downloadURL");
        showLoader(false);
        setState(() {});
      }
    } catch (e) {
      showLoader(false);
    }
  }

  showLoader(bool show) {
    if (show)
      showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) => Loader());
    else
      Navigator.pop(context);
  }
}

showLogoutAlert(BuildContext context) {
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        title: Text(
          'Logout',
          textAlign: TextAlign.center,
          style: styleAlertTitle,
        ),
        content: Text(
          'Are you want to logout?',
          textAlign: TextAlign.center,
          style: styleAlertTitle,
        ),
        actions: <Widget>[
          Container(
            height: 0.3,
            color: const Color(0xFF707070),
            child: SizedBox(
              width: 500,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(top: 15),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'NO',
                      style: styleAlertYes,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  color: Colors.white,
                  height: 50,
                ),
              ),
              Container(
                height: 40,
                color: const Color(0xFF707070),
                child: SizedBox(
                  width: 0.5,
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(top: 15),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop(); //close the drawer
                      logOut(context);
                    },
                    child: Text(
                      'YES',
                      style: styleAlertNo,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  color: Colors.white,
                  height: 50,
                ),
              ),
            ],
          ),
        ],
      );
    },
  );
}

logOut(BuildContext context) {
  FirebaseAuth.instance.signOut();
  SharedPreferencesUtil.clear();
  Navigator.pushAndRemoveUntil(
    context,
    MaterialPageRoute(
      builder: (BuildContext context) => SendOtpScreen(),
    ),
    (route) => false,
  );
}

showLoginAlert(BuildContext context) {
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        title: Text(
          'Login',
          textAlign: TextAlign.center,
          style: styleAlertTitle,
        ),
        content: Text(
          'Are you want to login?',
          textAlign: TextAlign.center,
          style: styleAlertTitle,
        ),
        actions: <Widget>[
          Container(
            height: 0.3,
            color: const Color(0xFF707070),
            child: SizedBox(
              width: 500,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(top: 15),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'NO',
                      style: styleAlertYes,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  color: Colors.white,
                  height: 50,
                ),
              ),
              Container(
                height: 40,
                color: const Color(0xFF707070),
                child: SizedBox(
                  width: 0.5,
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(top: 15),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SendOtpScreen()),
                      );
                    },
                    child: Text(
                      'YES',
                      style: styleAlertNo,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  color: Colors.white,
                  height: 50,
                ),
              ),
            ],
          ),
        ],
      );
    },
  );
}
