import 'package:PinoyHelpHotline/contansts/color_constant.dart';
import 'package:PinoyHelpHotline/contansts/constant.dart';
import 'package:PinoyHelpHotline/contansts/style_constant.dart';
import 'package:PinoyHelpHotline/custom/item_widget.dart';
import 'package:PinoyHelpHotline/util/sharedpreference_util.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:logger/logger.dart';

class ItemListScreen extends StatefulWidget {
  @override
  ItemListScreenState createState() => ItemListScreenState();
}

class ItemListScreenState extends State<ItemListScreen>
    with TickerProviderStateMixin, WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  late BuildContext myContext;
  int _currentTab = 0;
  late TabController _tabController;
  int index = 0;
  var logger = Logger();
  FirebaseAuth auth = FirebaseAuth.instance;

  List<DocumentSnapshot> categoryList = [];
  bool isCategoryLoaded = false;
  List<String> selectedItems = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadCategory();
  }

  loadCategory() async {
    selectedItems.addAll(
        SharedPreferencesUtil.getStringList(ConstantUtil.PREF_FAV_ITEMS));
    var categories = await FirebaseFirestore.instance
        .collection('categories')
        .orderBy("name", descending: false)
        .get();

    categoryList.addAll(categories.docs);

    _tabController =
        new TabController(length: categoryList.length, vsync: this);

    setState(() {
      isCategoryLoaded = true;
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        //or set color with: Color(0xFF0000FF)
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark));
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, top: 60),
            child: Container(
                child: Text(
              "Let's",
              style: styleLetsTitle,
            )),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, bottom: 8),
            child: Container(
                child: Text(
              "Get's started!",
              style: styleGetTitle,
            )),
          ),
          isCategoryLoaded
              ? Container(
                  padding: EdgeInsets.only(left: 10, right: 10, top: 12),
                  child: Container(
                    height: 30,
                    child: TabBar(
                      unselectedLabelColor: Colors.black87,
                      physics: BouncingScrollPhysics(),
                      indicator: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                          color: ColorUtil.colorPrimary),
                      labelColor: Colors.white,
                      isScrollable: true,
                      indicatorColor: Colors.grey,
                      tabs: List<Widget>.generate(categoryList.length,
                          (int index) {
                        return new Tab(text: categoryList[index]["name"]);
                      }),
                      controller: _tabController,
                      onTap: (index) {
                        _currentTab = index;
                        setState(() {});
                      },
                    ),
                  ),
                )
              : new Container(),
          Expanded(
            child: Container(
              margin: const EdgeInsets.only(left: 10, right: 10),
              child: isCategoryLoaded
                  ? StreamBuilder(
                      stream: FirebaseFirestore.instance
                          .collection("posts")
                          .where(ConstantUtil.FIREBASE_KEY_STATUS,
                              isEqualTo: ConstantUtil.RECORD_ACTIVE)
                          .where("categoryId",
                              isEqualTo: categoryList[_currentTab]["id"])
                          .orderBy('updatedAt', descending: true)
                          .snapshots(),
                      builder: (BuildContext context,
                          AsyncSnapshot<QuerySnapshot<Map<String, dynamic>>>
                              snapshot) {
                        if (snapshot.hasData) {
                          logger.e(snapshot.data!.docs.length);
                          return snapshot.data!.docs.length == 0
                              ? Center(
                                  child: Text(
                                    "No data available.",
                                    style:
                                        fontStyleNormal.copyWith(fontSize: 20),
                                  ),
                                )
                              : Container(
                                  padding: const EdgeInsets.all(5),
                                  child: Padding(
                                    padding: const EdgeInsets.all(1.0),
                                    child: GridView.count(
                                        crossAxisCount: 2,
                                        crossAxisSpacing: 8.0,
                                        mainAxisSpacing: 8.0,
                                        childAspectRatio: (1 / 1.3),
                                        children: List.generate(
                                            snapshot.data!.docs.length,
                                            (index) {
                                          DocumentSnapshot documentSnapshot =
                                              snapshot.data!.docs[index];
                                          return ItemWidget(
                                              documentSnapshot:
                                                  documentSnapshot,
                                              index: index,
                                              onClick: (id) {
                                                checkFavItem(id);
                                              });
                                        })),
                                  ),
                                );
                        }
                        return Center(
                            child: CircularProgressIndicator(
                          color: ColorUtil.colorSecondary,
                        ));
                      },
                    )
                  : Center(
                      child: CircularProgressIndicator(
                      color: ColorUtil.colorSecondary,
                    )),
            ),
          ),
        ],
      ),
    );
  }

  checkFavItem(String id) {
    List<String> temp =
        SharedPreferencesUtil.getStringList(ConstantUtil.PREF_FAV_ITEMS);
    var index = temp.indexOf(id);

    if (index == -1)
      temp.add(id);
    else
      temp.removeAt(index);

    SharedPreferencesUtil.setStringList(ConstantUtil.PREF_FAV_ITEMS, temp);

    setState(() {});
  }
}
