import 'package:PinoyHelpHotline/contansts/color_constant.dart';
import 'package:PinoyHelpHotline/contansts/constant.dart';
import 'package:PinoyHelpHotline/contansts/style_constant.dart';
import 'package:PinoyHelpHotline/custom/my_item_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';

class MyAdsScreen extends StatefulWidget {
  @override
  MyAdsScreenState createState() => MyAdsScreenState();
}

class MyAdsScreenState extends State<MyAdsScreen>
    with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  late BuildContext myContext;
  var logger = Logger();
  var bseData, nseData;
  FirebaseAuth auth = FirebaseAuth.instance;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        brightness: Brightness.light,
        centerTitle: true,
        backgroundColor: ColorUtil.colorPrimary,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back_ios_rounded,
            color: ColorUtil.colorWhite,
          ),
        ),
        title: Text(
          "My Ads",
          style: styleTitle,
        ),
        elevation: 3,
        automaticallyImplyLeading: false,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Container(
              margin: const EdgeInsets.only(top: 5, left: 10, right: 10),
              child: StreamBuilder(
                stream: FirebaseFirestore.instance
                    .collection("posts")
                    .where(ConstantUtil.FIREBASE_KEY_STATUS,
                        isEqualTo: ConstantUtil.RECORD_ACTIVE)
                    .orderBy('updatedAt', descending: true)
                    .snapshots(),
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot<Map<String, dynamic>>>
                        snapshot) {
                  if (snapshot.hasData) {
                    logger.e(snapshot.data!.docs.length);
                    return Container(
                      padding: const EdgeInsets.all(5),
                      child: Padding(
                        padding: const EdgeInsets.all(1.0),
                        child: GridView.count(
                            crossAxisCount: 2,
                            crossAxisSpacing: 8.0,
                            mainAxisSpacing: 8.0,
                            childAspectRatio: (1 / 1.1),
                            children: List.generate(snapshot.data!.docs.length,
                                (index) {
                              DocumentSnapshot documentSnapshot =
                                  snapshot.data!.docs[index];

                              return MyItemWidget(
                                  documentSnapshot: documentSnapshot,
                                  index: index,
                                  onClick: (id) {});
                            })),
                      ),
                    );
                  }
                  return Center(
                      child: CircularProgressIndicator(
                    color: ColorUtil.colorSecondary,
                  ));
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
