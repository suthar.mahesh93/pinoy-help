import 'package:PinoyHelpHotline/contansts/color_constant.dart';
import 'package:PinoyHelpHotline/contansts/constant.dart';
import 'package:PinoyHelpHotline/contansts/style_constant.dart';
import 'package:PinoyHelpHotline/custom/item_widget.dart';
import 'package:PinoyHelpHotline/util/sharedpreference_util.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';

class WishListScreen extends StatefulWidget {
  @override
  WishListScreenState createState() => WishListScreenState();
}

class WishListScreenState extends State<WishListScreen>
    with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  late BuildContext myContext;
  var logger = Logger();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: ColorUtil.colorPrimary,
        title: Text(
          "Favorite",
          style: styleTitle,
        ),
        elevation: 3,
        automaticallyImplyLeading: false,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Container(
              margin: const EdgeInsets.only(top: 5, left: 10, right: 10),
              child: StreamBuilder(
                stream: FirebaseFirestore.instance
                    .collection("posts")
                    .where(ConstantUtil.FIREBASE_KEY_STATUS,
                        isEqualTo: ConstantUtil.RECORD_ACTIVE)
                    .orderBy('updatedAt', descending: true)
                    .snapshots(),
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot<Map<String, dynamic>>>
                        snapshot) {
                  if (snapshot.hasData) {
                    logger.e(snapshot.data!.docs.length);
                    List<DocumentSnapshot> data = getData(snapshot.data!.docs);
                    return Container(
                      padding: const EdgeInsets.all(5),
                      child: Padding(
                        padding: const EdgeInsets.all(1.0),
                        child: GridView.count(
                            crossAxisCount: 2,
                            crossAxisSpacing: 8.0,
                            mainAxisSpacing: 8.0,
                            childAspectRatio: (1 / 1.3),
                            children: List.generate(data.length, (index) {
                              DocumentSnapshot documentSnapshot = data[index];

                              return ItemWidget(
                                  documentSnapshot: documentSnapshot,
                                  index: index,
                                  onClick: (id) {
                                    checkFavItem(id);
                                  });
                            })),
                      ),
                    );
                  }
                  return Center(
                      child: CircularProgressIndicator(
                    color: ColorUtil.colorSecondary,
                  ));
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  List<DocumentSnapshot> getData(List<DocumentSnapshot> list) {
    return list
        .where((product) =>
            SharedPreferencesUtil.getStringList(ConstantUtil.PREF_FAV_ITEMS)
                .contains(product["id"]))
        .toList();
  }

  checkFavItem(String id) {
    List<String> temp =
        SharedPreferencesUtil.getStringList(ConstantUtil.PREF_FAV_ITEMS);
    var index = temp.indexOf(id);

    if (index == -1)
      temp.add(id);
    else
      temp.removeAt(index);

    SharedPreferencesUtil.setStringList(ConstantUtil.PREF_FAV_ITEMS, temp);

    setState(() {});
  }
}
