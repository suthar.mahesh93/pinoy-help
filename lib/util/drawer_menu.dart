import 'package:PinoyHelpHotline/contansts/color_constant.dart';
import 'package:PinoyHelpHotline/contansts/style_constant.dart';
import 'package:PinoyHelpHotline/model/drawer_item.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';


showLogoutAlert(BuildContext context) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        title: Text(
          'Logout',
          textAlign: TextAlign.center,
          style: styleAlertTitle,
        ),
        content: Text(
          'Are you want to logout?',
          textAlign: TextAlign.center,
          style: styleAlertTitle,
        ),
        actions: <Widget>[
          Container(
            height: 0.3,
            color: const Color(0xFF707070),
            child: SizedBox(
              width: 500,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(top: 15),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'NO',
                      style: styleAlertYes,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  color: Colors.white,
                  height: 50,
                ),
              ),
              Container(
                height: 40,
                color: const Color(0xFF707070),
                child: SizedBox(
                  width: 0.5,
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(top: 15),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop(); //close the drawer
                      //logOut(context);
                    },
                    child: Text(
                      'YES',
                      style: styleAlertNo,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  color: Colors.white,
                  height: 50,
                ),
              ),
            ],
          ),
        ],
      );
    },
  );
}

/*
logOut(BuildContext context) {
  FirebaseAuth.instance.signOut();
  SharedPreferencesUtil.clear();
  Navigator.pushAndRemoveUntil(
    context,
    MaterialPageRoute(
      builder: (BuildContext context) => SendOtpScreen(),
    ),
        (route) => false,
  );
}
*/

showToast(String msg) {
  Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: ColorUtil.colorSecondary,
      textColor: Colors.white,
      fontSize: 16.0);
}
