import 'package:PinoyHelpHotline/contansts/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesUtil {
  static late SharedPreferences _prefs;

  static Future<bool> init() async {
    _prefs = await SharedPreferences.getInstance();
    return true;
  }

  static setBoolean(String key, bool value) {
    return _prefs.setBool(key, value);
  }

  static bool getBoolean(String key) {
    return _prefs.getBool(key) ?? false;
  }

  static setString(String key, String value) {
    _prefs.setString(key, value);
  }

  static String getString(String key) {
    return _prefs.getString(key) ?? "";
  }

  static setInt(String key, int value) {
    _prefs.setInt(key, value);
  }

  static int getInt(String key) {
    return _prefs.getInt(key) ?? 0;
  }

  static String getFirstCharFromName() {
    if (getString(ConstantUtil.PREF_FNAME).isEmpty) return "";

    return getString(ConstantUtil.PREF_FNAME)[0] +
        getString(ConstantUtil.PREF_LNAME)[0];
  }

  static setStringList(String key, List<String> value) {
    _prefs.setStringList(key, value);
  }

  static List<String> getStringList(String key) {
    return _prefs.getStringList(key) ?? [];
  }

  static Future clear() async {
    _prefs.clear();
  }
}
