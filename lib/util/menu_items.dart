import 'package:PinoyHelpHotline/model/currency.dart';
import 'package:flutter/material.dart';

List<DropdownMenuItem<String>> getDropdownList(List<String> list) {
  List<DropdownMenuItem<String>> items = [];
  for (String currency in list) {
    items.add(DropdownMenuItem(
      value: currency,
      child: Text(currency),
    ));
  }
  return items;
}

List<DropdownMenuItem<CurrencyItem>> getDropdownCurrencyList(
    List<CurrencyItem> list) {
  List<DropdownMenuItem<CurrencyItem>> items = [];
  for (CurrencyItem currency in list) {
    items.add(DropdownMenuItem(
      value: currency,
      child: Text(currency.currency),
    ));
  }
  return items;
}

List<CurrencyItem> getCurrency() {
  List<CurrencyItem> items = [];
  items.add(new CurrencyItem(currency: "ALL", code: "Lek"));
  items.add(new CurrencyItem(currency: "AFN", code: "؋"));
  items.add(new CurrencyItem(currency: "ARS", code: "\$"));
  items.add(new CurrencyItem(currency: "AWG", code: "ƒ"));
  items.add(new CurrencyItem(currency: "AUD", code: "\$"));
  items.add(new CurrencyItem(currency: "AZN", code: "₼"));
  items.add(new CurrencyItem(currency: "BSD", code: "\$"));
  items.add(new CurrencyItem(currency: "BBD", code: "\$"));
  items.add(new CurrencyItem(currency: "BYN", code: "Br"));
  items.add(new CurrencyItem(currency: "BZD", code: "BZ\$"));
  items.add(new CurrencyItem(currency: "BMD", code: "\$"));
  items.add(new CurrencyItem(currency: "BOB", code: "\$b"));
  items.add(new CurrencyItem(currency: "BAM", code: "KM"));
  items.add(new CurrencyItem(currency: "BWP", code: "	P"));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  // items.add(new CurrencyItem(currency: "", code: ""));
  return items;
}
